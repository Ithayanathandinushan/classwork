public class SwitchCaseDemo{
   public static void main (String arg[]){
       int num = 7 ;
	  switch (num){
	    
		  case 1 :
		      System.out.println( "case1 : value is : " + num ) ;
			  break;
		  case 2 :
		      System.out.println( "case2 : value is : " + num ) ;
			  break; 
		  case 3 :
		      System.out.println( "case3 : value is : " + num ) ;
			  break;
		  default :
		      System.out.println( "default: value is : " + num ) ;
			  break;
	  }
   }
}